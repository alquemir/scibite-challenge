import React, { useState, useEffect } from 'react';
import { debounce } from 'lodash';
import { ApiResponse } from '@elastic/elasticsearch';
import { SearchBar } from './search-bar';
import SearchResults from './search-results';
import { Pagination } from './pagination';
import DocumentsApi from './backend/documents-api';

export default function AppContainer(): JSX.Element {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalResults, setTotalResults] = useState(0);
  const [resultsPerPage] = useState(5);
  const [isLoading, setIsLoading] = useState(false);
  const [searchError, setSearchError] = useState('');

  useEffect(
    debounce(() => {
      findDocuments().then(() => {
        setCurrentPage(1);
      });
    }, 200),
    [searchTerm]
  );

  useEffect(() => {
    findDocuments().then(() => {
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    });
  }, [currentPage]);

  function findDocuments(): Promise<void> {
    setIsLoading(true);
    setSearchError('');

    return new Promise((resolve, reject) => {
      DocumentsApi.findDocuments(searchTerm, currentPage, resultsPerPage)
        .then(({ body }: ApiResponse<any>) => {
          setSearchResults(body.hits.hits);
          setTotalResults(body.hits.total);
          setIsLoading(false);
          resolve();
        })
        .catch(error => {
          setSearchError(error);
          reject();
        });
    });
  }

  function handleSearch(searchTerm: string): void {
    setSearchTerm(searchTerm);
  }

  function getSearchPlaceholder(): string {
    return searchTerm ? searchTerm : 'Enter search criteria ...';
  }

  function handlePageClick(page: number): void {
    setCurrentPage(page);
  }

  function isPaginationVisible(): boolean {
    return !searchError && !isLoading;
  }

  return (
    <div className="app-container bp3-dark">
      <header className="app-container__header">
        <SearchBar
          searchTerm={searchTerm}
          searchPlaceholder={getSearchPlaceholder()}
          onSearch={(searchTerm: string) => handleSearch(searchTerm)}
        />
      </header>
      <main>
        <SearchResults
          searchResults={searchResults}
          resultsPerPage={resultsPerPage}
          totalResults={totalResults}
          currentPage={currentPage}
          isLoading={isLoading}
          error={searchError}
        />
      </main>
      <footer>
        {isPaginationVisible() && (
          <Pagination
            totalResults={totalResults}
            currentPage={currentPage}
            resultsPerPage={resultsPerPage}
            maxPages={10}
            onFirstPageClick={handlePageClick}
            onPreviousPageClick={handlePageClick}
            onPageClick={handlePageClick}
            onNextPageClick={handlePageClick}
            onLastPageClick={handlePageClick}
          />
        )}
      </footer>
    </div>
  );
}
