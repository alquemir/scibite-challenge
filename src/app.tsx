import React from 'react';
import AppContainer from './app-container';
import './app.css';

export default function App(): JSX.Element {
  return (
    <div className="app">
      <AppContainer />
    </div>
  );
}
