import { ApiResponse } from '@elastic/elasticsearch';

const DocumentsApi = (() => {
  return {
    findDocuments: async (
      searchTerm: string = '',
      fromPage: number = 1,
      pageSize: number = 10
    ): Promise<ApiResponse<any>> => {
      return fetch(
        `/api/search?searchTerm=${searchTerm}&fromPage=${fromPage}&pageSize=${pageSize}`
      )
        .then(response => response.json())
        .then((data: any) => Promise.resolve(data));
    }
  };
})();

export default DocumentsApi;
