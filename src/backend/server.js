const fs = require('fs');
const { Client } = require('@elastic/elasticsearch');
const Hapi = require('@hapi/hapi');

const Server = (() => {
  function startElasticSearchClient() {
    return new Client({
      node: 'https://scidbtest.scibite.com:9243',
      auth: {
        username: 'can3',
        password: '43wfggTki'
      },
      ssl: {
        ca: fs.readFileSync(__dirname + '/cert.pem'),
        rejectUnauthorized: false
      }
    });
  }

  function findDocuments(client, searchTerm = '', fromPage = 1, pageSize = 10) {
    return new Promise((resolve, reject) => {
      client
        .search({
          index: 'scidb',
          method: 'GET',
          body: {
            from: +fromPage,
            size: +pageSize,
            query: {
              query_string: {
                query: searchTerm
              }
            },
            highlight: {
              tags_schema: 'styled',
              fields: {
                '*': {}
              }
            }
          }
        })
        .then(response => {
          console.log(response);
          resolve(response);
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  }

  function startRestServer() {
    const client = startElasticSearchClient();
    const server = Hapi.server({
      host: 'localhost',
      port: '4000'
    });

    server.route({
      method: 'GET',
      path: '/api/ping',
      handler: () => 'pong ...'
    });

    server.route({
      method: 'GET',
      path: '/api/search',
      handler: async request => {
        const { searchTerm, fromPage, pageSize } = request.query;
        return findDocuments(client, searchTerm, fromPage, pageSize);
      }
    });

    return server
      .start()
      .then(() => {
        console.log('Server running on %ss', server.info.uri);
      })
      .catch(error => {
        console.log('Server error: ', error);
        process.exit(1);
      });
  }

  startRestServer();
})();

module.exports = Server;
