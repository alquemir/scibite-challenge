import React from 'react';
import { uniq } from 'lodash';

export interface HighlightProps {
  text: string;
}

export function Highlight({ text }: HighlightProps): JSX.Element {
  function getTextWithoutLineBreaks(input: string): string {
    return input.replace(/(\r\n|\n|\r)/gim, '');
  }

  function getWithoutTag(input: string): string {
    return input.replace(/<\/?em.*?>/gi, '');
  }

  function getTaggedText(input: string): string[] {
    const tags = uniq(input.match(/(<em.*?>\w+<\/em>)/gi));
    return tags.map(tag => getWithoutTag(tag));
  }

  function getHighlightMarkup(): Array<string | JSX.Element> {
    const input = getTextWithoutLineBreaks(text);
    const tags = getTaggedText(input);
    return input
      .split(/(<em.*?>\w+<\/em>)/gi)
      .map(value => getWithoutTag(value))
      .map((value, index) =>
        tags.includes(value) ? (
          <mark key={`tag-${index + 1}`}>{value}</mark>
        ) : (
          value
        )
      );
  }

  return <div className="highlight">{getHighlightMarkup()}</div>;
}
