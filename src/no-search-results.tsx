import React from 'react';
import { NonIdealState } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

export default function NoSearchResults(): JSX.Element {
  function getDescription(): JSX.Element {
    return (
      <>
        Your search didn't match any files.
        <br />
        Try searching for something else.
      </>
    );
  }

  return (
    <div className="no-search-results">
      <NonIdealState
        title="No search results"
        icon={IconNames.SEARCH}
        description={getDescription()}
      />
    </div>
  );
}
