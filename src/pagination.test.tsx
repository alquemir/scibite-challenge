import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Pagination, PaginationProps } from './pagination';

configure({ adapter: new Adapter() });

describe('Pagination', () => {
  it('it should display the correct page buttons after the current page changes towards start', () => {
    const props: PaginationProps = {
      resultsPerPage: 10,
      totalResults: 150,
      currentPage: 1,
      maxPages: 10,
      onNextPageClick: jest.fn(),
      onPreviousPageClick: jest.fn(),
      onPageClick: jest.fn(),
      onFirstPageClick: jest.fn(),
      onLastPageClick: jest.fn()
    };

    const instance = mount(<Pagination {...props} />);
    const nodes = instance.find('button.pagination__page');

    expect(nodes.length).toEqual(10);
    expect(nodes.first().text()).toEqual('1');
    expect(nodes.last().text()).toEqual('10');
  });

  it('it should display the correct page buttons after the current page changes towards middle', () => {
    const props: PaginationProps = {
      resultsPerPage: 10,
      totalResults: 150,
      currentPage: 7,
      maxPages: 10,
      onNextPageClick: jest.fn(),
      onPreviousPageClick: jest.fn(),
      onPageClick: jest.fn(),
      onFirstPageClick: jest.fn(),
      onLastPageClick: jest.fn()
    };

    const instance = mount(<Pagination {...props} />);
    const nodes = instance.find('button.pagination__page');

    expect(nodes.length).toEqual(10);
    expect(nodes.first().text()).toEqual('2');
    expect(nodes.last().text()).toEqual('11');
  });

  it('it should display the correct page buttons after the current page changes towards end', () => {
    const props: PaginationProps = {
      resultsPerPage: 10,
      totalResults: 150,
      currentPage: 11,
      maxPages: 10,
      onNextPageClick: jest.fn(),
      onPreviousPageClick: jest.fn(),
      onPageClick: jest.fn(),
      onFirstPageClick: jest.fn(),
      onLastPageClick: jest.fn()
    };

    const instance = mount(<Pagination {...props} />);
    const nodes = instance.find('button.pagination__page');

    expect(nodes.length).toEqual(10);
    expect(nodes.first().text()).toEqual('6');
    expect(nodes.last().text()).toEqual('15');
  });
});
