import React from 'react';
import { range } from 'lodash';
import { Button } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

export interface PaginationProps {
  resultsPerPage: number;
  totalResults: number;
  currentPage: number;
  maxPages: number;
  onNextPageClick: (page: number) => void;
  onPreviousPageClick: (page: number) => void;
  onPageClick: (page: number) => void;
  onFirstPageClick: (page: number) => void;
  onLastPageClick: (page: number) => void;
}

export function Pagination(props: PaginationProps): JSX.Element | null {
  const {
    resultsPerPage,
    totalResults,
    currentPage,
    maxPages,
    onNextPageClick,
    onPreviousPageClick,
    onPageClick,
    onFirstPageClick,
    onLastPageClick
  } = props;

  function handleNextPageClick(): void {
    onNextPageClick(currentPage + 1);
  }

  function handlePreviousPageClick(): void {
    onPreviousPageClick(currentPage - 1);
  }

  function handlePageClick(page: number): void {
    onPageClick(page);
  }

  function handleFirstPageClick(): void {
    onFirstPageClick(1);
  }

  function handleLastPageClick(): void {
    onLastPageClick(getTotalPages());
  }

  function getTotalPages(): number {
    return Math.ceil(totalResults / resultsPerPage);
  }

  function getPagesBeforeCurrentPage(): number {
    return Math.floor(maxPages / 2);
  }

  function getPagesAfterCurrentPage(): number {
    return Math.ceil(maxPages / 2);
  }

  function getStartPage(): number {
    const totalPages = getTotalPages();
    const pagesBeforeCurrentPage = getPagesBeforeCurrentPage();
    const pagesAfterCurrentPage = getPagesAfterCurrentPage();

    if (currentPage <= pagesBeforeCurrentPage || totalPages <= maxPages) {
      return 1;
    }

    if (currentPage + pagesAfterCurrentPage >= totalPages) {
      return totalPages - maxPages + 1;
    }

    return currentPage - pagesBeforeCurrentPage;
  }

  function getEndPage(): number {
    const totalPages = getTotalPages();
    const pagesBeforeCurrentPage = getPagesBeforeCurrentPage();
    const pagesAfterCurrentPage = getPagesAfterCurrentPage();

    if (totalPages <= maxPages) {
      return totalPages + 1;
    }

    if (currentPage <= pagesBeforeCurrentPage) {
      return maxPages + 1;
    }

    if (currentPage + pagesAfterCurrentPage >= totalPages) {
      return totalPages + 1;
    }

    return currentPage + pagesAfterCurrentPage;
  }

  function getPaginationRange(): number[] {
    return range(getStartPage(), getEndPage());
  }

  function isVisible(): boolean {
    return totalResults > resultsPerPage;
  }

  function isActiveButton(page: number): boolean {
    return page === currentPage;
  }

  function isNextPageButtonVisible(): boolean {
    return currentPage <= getTotalPages() - getPagesAfterCurrentPage();
  }

  function isPreviousPageButtonVisible(): boolean {
    return currentPage > getPagesBeforeCurrentPage() + 1;
  }

  return isVisible() ? (
    <div className="pagination">
      <Button
        onClick={handleFirstPageClick}
        className="pagination__first"
        icon={IconNames.DOUBLE_CHEVRON_LEFT}
      />
      {isPreviousPageButtonVisible() && (
        <Button
          onClick={handlePreviousPageClick}
          className="pagination__previous"
          icon={IconNames.CHEVRON_LEFT}
        />
      )}
      {getPaginationRange().map(page => (
        <Button
          key={page}
          active={isActiveButton(page)}
          className="pagination__page"
          onClick={() => handlePageClick(page)}
        >
          {page}
        </Button>
      ))}
      {isNextPageButtonVisible() && (
        <Button
          onClick={handleNextPageClick}
          className="pagination__next"
          icon={IconNames.CHEVRON_RIGHT}
        />
      )}
      <Button
        onClick={handleLastPageClick}
        className="pagination__last"
        icon={IconNames.DOUBLE_CHEVRON_RIGHT}
      />
    </div>
  ) : null;
}
