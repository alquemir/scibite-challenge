import React from 'react';
import { Navbar, InputGroup, Alignment, Keys } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

export interface SearchBarProps {
  searchTerm: string;
  searchPlaceholder: string;
  onSearch: (searchTerm: string) => void;
}

export function SearchBar(props: SearchBarProps): JSX.Element {
  const { searchTerm, onSearch, searchPlaceholder } = props;

  function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
    onSearch(event.currentTarget.value);
  }

  function handleKeyDown(event: React.KeyboardEvent<HTMLInputElement>): void {
    if (event.keyCode === Keys.ENTER) {
      onSearch(event.currentTarget.value);
    }
  }

  return (
    <div className="search-bar">
      <Navbar fixedToTop={true}>
        <Navbar.Group align={Alignment.CENTER} className="search-bar__group">
          <InputGroup
            onChange={handleChange}
            onKeyDown={handleKeyDown}
            value={searchTerm}
            placeholder={searchPlaceholder}
            large={true}
            fill={true}
            leftIcon={IconNames.SEARCH}
            type="search"
          />
        </Navbar.Group>
      </Navbar>
    </div>
  );
}
