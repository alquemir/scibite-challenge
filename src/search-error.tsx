import React from 'react';
import { NonIdealState } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

export interface SearchErrorProps {
  error: string;
}

export function SearchError({ error }: SearchErrorProps): JSX.Element {
  return (
    <div className="search-error">
      <NonIdealState
        title="An error has occured"
        description={error}
        icon={IconNames.WARNING_SIGN}
      />
    </div>
  );
}
