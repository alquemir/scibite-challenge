import React from 'react';
import { toPairs } from 'lodash';
import { Highlight } from './highlight';

export interface SearchResultMatchesProps {
  matches: any;
}

export function SearchResultMatches(
  props: SearchResultMatchesProps
): JSX.Element {
  const { matches } = props;

  function getTableRows(): JSX.Element[] {
    return toPairs(matches).map((pair, index) => (
      <tr key={index}>
        <td key={`field-${index + 1}`}>{pair[0]}</td>
        <td key={`value-${index + 1}`}>
          <Highlight text={String(pair[1])} />
        </td>
      </tr>
    ));
  }

  return (
    <div className="search-result-matches">
      <table className="bp3-html-table">
        <thead>
          <tr>
            <th>Field</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>{getTableRows()}</tbody>
      </table>
    </div>
  );
}
