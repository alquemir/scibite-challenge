import React from 'react';
import { Card, Elevation, Tag } from '@blueprintjs/core';
import { SearchResultMatches } from './search-result-matches';

export interface SearchResultProps {
  searchResult: any;
}

export function SearchResult({ searchResult }: SearchResultProps): JSX.Element {
  const { _source: document, highlight } = searchResult;

  function getDocumentTitle(): string {
    return document.fromName || document.from;
  }

  return (
    <div className="search-result">
      <Card elevation={Elevation.ONE}>
        <h3 className="search-result__title">
          <a href={document.url} target="_blank">
            {getDocumentTitle()}
          </a>
        </h3>
        <SearchResultMatches matches={highlight} />
        <div className="search-result__footer">
          <Tag minimal={true}>{document.datasetName}</Tag>
        </div>
      </Card>
    </div>
  );
}
