import React from 'react';

export interface SearchResultsInfoProps {
  resultsPerPage: number;
  totalResults: number;
  currentPage: number;
}

export function SearchResultsInfo(props: SearchResultsInfoProps): JSX.Element {
  const { resultsPerPage, totalResults, currentPage } = props;

  function getFromSearchResults(): number {
    return currentPage === 1 ? 1 : resultsPerPage * (currentPage - 1);
  }

  function getToSearchResults(): number {
    return resultsPerPage * currentPage;
  }

  return (
    <div className="search-results-info">
      <h5>
        Showing {getFromSearchResults()} - {getToSearchResults()} of{' '}
        {totalResults} results
      </h5>
    </div>
  );
}
