import React from 'react';
import { SearchResult } from './search-result';
import { SearchResultsInfo } from './search-results-info';
import NoSearchResults from './no-search-results';
import {
  withLoadingAndErrorState,
  WithLoadingAndErrorStateProps
} from './with-loading-and-error-state';

export interface SearchResultsProps extends WithLoadingAndErrorStateProps {
  searchResults: any[];
  resultsPerPage: number;
  totalResults: number;
  currentPage: number;
}

function SearchResults(props: SearchResultsProps): JSX.Element {
  const { searchResults, resultsPerPage, currentPage, totalResults } = props;

  return (
    <div className="search-results">
      {searchResults.length > 0 && (
        <SearchResultsInfo
          resultsPerPage={resultsPerPage}
          totalResults={totalResults}
          currentPage={currentPage}
        />
      )}
      {searchResults.length > 0 ? (
        searchResults.map(searchResult => (
          <SearchResult key={searchResult._id} searchResult={searchResult} />
        ))
      ) : (
        <NoSearchResults />
      )}
    </div>
  );
}

export default withLoadingAndErrorState(SearchResults);
