import React from 'react';
import { Spinner } from '@blueprintjs/core';
import { SearchError } from './search-error';

export interface WithLoadingAndErrorStateProps {
  isLoading: boolean;
  error: string;
}

export function withLoadingAndErrorState<
  P extends WithLoadingAndErrorStateProps
>(WrappedComponent: React.FC<P>): React.FC<P & WithLoadingAndErrorStateProps> {
  return (props: P & WithLoadingAndErrorStateProps): JSX.Element => {
    const { isLoading, error, ...rest } = props;

    if (error) return <SearchError error={error} />;

    return isLoading ? (
      <Spinner className="loader" />
    ) : (
      <WrappedComponent {...(rest as P)} />
    );
  };
}
